# Source Generators Test

## Description

This project is a practical test of C#'s Source Generators, specifically for registering UI View deep links within a mock UI system. The focus is on assessing how effectively these generators can streamline development processes in scenarios that require the exchange of generic or custom data types for communication.

By employing source generators, we aim to simplify the creation of new classes for the UI system. Developers will be able to seamlessly register these classes without directly altering the system. This setup not only maintains the integrity of the type system within their APIs but also allows for handling complex data structures, including generic methods, in client code APIs. The ultimate goal is a compile-time, secure method of interfacing with the system, enhancing both efficiency and reliability.

TL;DR: It's a test of using source generators to register UI View deep links for a imaginary UI system.

## References

* https://github.com/dotnet/roslyn/blob/main/docs/features/source-generators.cookbook.md
* https://medium.com/@EnescanBektas/using-source-generators-in-the-unity-game-engine-140ff0cd0dc
* https://docs.unity3d.com/Manual/roslyn-analyzers.html
* https://chat.openai.com/

## How the test is structured

This test simulates a basic UI system managed through a UI manager, capable of launching various UI Views. A notable aspect of this system is its handling of certain views that necessitate complex, generic view data classes.

At the core is the `BaseView` class, which is generic and accepts a data class:

``````csharp
public abstract class BaseView<T> : MonoBehaviour, IView where T : BaseViewData
{
    public abstract void Setup(T data);
    public GameObject Owner => gameObject;
}
``````

The UI Manager is engineered to open views according to their types, establishing compile-time safeguards for both the view and its associated data class:
``````csharp
public void OpenView<T, TData>(TData data) 
            where T : BaseView<TData>
            where TData : BaseViewData
{
    Type viewType = typeof(T);
    //...
    var prefab = _viewPrefabs[viewType];
    var newView = Instantiate(prefab, viewsRoot);
    var view = newView.GetComponent<T>();
    //...
    view.Setup(data);
	//...
}
``````

In this setup, the `OpenView` method in the UI Manager dynamically handles views. It instantiates them based on their type, ensuring that the views and their data classes are correctly aligned and safely managed at compile-time. This approach significantly reduces the risk of runtime errors related to type mismatches, enhancing the robustness of the UI system.

## The challenge

The primary challenge in this setup is integrating a Deep Link system into the proposed UI framework. This system is complex because it demands one of three approaches: reflection, serialization manipulation, or manual registration of view creation methods. The complexity arises due to the nature of deep linking, where both the view type and its parameters are represented as strings. Consequently, there's a need for a precise mapping between these strings and the actual view types and parameters.

 To illustrate this, a manual mapping class is included in the test:
``````csharp
public static class ManualDeepLinkRegistry
    {
        public static void OpenFromTypeName(UIManager manager, string typeName, Dictionary<string, string> parameters)
        {
            string lowerTypeName = typeName.ToLower();

            if (lowerTypeName == nameof(ViewA).ToLower())
            {
                OpenViewA(manager, parameters);
                return;
            }
            
            // other if-else methods...
        }

        private static void OpenViewA(UIManager manager, IReadOnlyDictionary<string, string> parameters)
        {
            //..
            int value = int.Parse(parameters["value"]);
            manager.OpenView<ViewA, ViewAData>(new ViewAData(value));
        }
        
        //...
}
``````

This class demonstrates the potential issue of scalability; as new views are added, the class would expand significantly. While there are techniques to mitigate this growth, like dispersing registration across the codebase, these solutions may not scale efficiently. The challenge lies in developing a robust system that can manage deep links without becoming unwieldy as the number of views increases.

## The generation solution

The innovative approach proposed here is to automate the mapping process for new classes, moving away from manual registration or the complexities of using reflection to create data classes from strings. This automation is achieved through the implementation of source generators, which dynamically generate a mapping class for each view at compile-time.

Source generators are employed to scan for a specific deep link attribute, extract class information, and then craft a corresponding mapping class. For instance, a view class tagged for deep link mapping would be defined as follows:

``````csharp
[DeepLink(ViewDataType = typeof(ViewAData))]
public class ViewA : BaseView<ViewAData>
{
    public TextMeshProUGUI valueText;
    public override void Setup(ViewAData data)
    {
        valueText.text = $"Value: {data.Value}";
    }
}
``````

This annotation leads to the automatic generation of a class like this:

``````csharp
public partial class DeepLinkRegistry
{
    protected static Dictionary<string, OpenViewAction> _openViewActions = new()
    {
            [nameof(GameUI.ViewA).ToLower()] = OpenViewA, //...
    };

    protected delegate void OpenViewAction(UISystem.UIManager manager, IReadOnlyDictionary<string, string> parameters);
    public static void OpenViewByTypeName(UISystem.UIManager manager, string viewTypeName, IReadOnlyDictionary<string, string> parameters)
    {
        //...
        _openViewActions[viewTypeName](manager, parameters);
    }
   
    public static void OpenViewA(UISystem.UIManager manager, IReadOnlyDictionary<string, string> parameters)
    {
        var value = int.Parse(parameters["value"]);
        manager.OpenView<GameUI.ViewA, GameUI.ViewAData>(new GameUI.ViewAData(value));
    }
    //...
}
``````

This generated class offers rapid execution and the potential for minimal memory overhead (though optimization for this was not a focus). Importantly, it upholds the type safety of the view data. The repository explores this challenge of creating a highly efficient, type-safe deep linking system in a UI framework.

## Considerations

### The Good

- The proposed method is highly effective and functional. While it requires some fine-tuning, it significantly enhances the development experience for client-side code.
- It effectively conceals the intricacies involved in deep link registration, significantly reducing the volume of code that developers need to manage. Moreover, it provides a system that automatically generates and oversees this code in a reliable manner.
- This approach substantially accelerates the development process.
- The generated code benefits not just the deep link system but all systems that interact with the affected classes.
- The system is optimized for speed due to its compile-time operation and the absence of reflection.
- There's a strong emphasis on type safety, ensuring that errors related to type mismatches are minimized.

### The Bad

* Unity's inability to compile code using Roslyn necessitates that the generator be a separate project, creating a DLL. This adds a layer of complexity to the development process.
* The isolated nature of this separate project means it lacks direct access to the original project's code. Crafting a solution to access necessary information from the original project is challenging and requires a creative setup.
* Due to the absence of Roslyn in Unity, the generated DLL cannot be directly integrated into the project. Consequently, the attribute class and any other user-facing classes must be generated separately, adding to the workload.
* Debugging proves to be a significant challenge. The lack of conventional printing or debug tools means developers must rely on either intricate mock setups for unit testing or the generation of log files.
* Handling types properly is difficult due to the inability to access Unity code/project directly. All types are derived from the syntax tree, leading to extensive hardcoding or syntax tree scanning.
* The process of generating code that instantiates view data classes is complex and multi-faceted:
  - It requires assembly scanning within the compilation unit to identify the correct view data class or struct.
    - Each view data type must be meticulously scanned for its parameters.
    - Parsing code must be created to convert strings (from the deep link) into the appropriate types. This limits support for complex types (like custom type arguments, params, arrays) without extensive parsing code generation or delegating this task to the partial class in the Unity project.
    - Generation involves a balancing act: either assuming the existence of classes on the Unity side based on string data, or scanning the Unity project during generation to understand the types involved.
  - Full qualified names of types must be identified through scanning.
* The introduction of this system might lead to, or has led to, slower assembly reloads, impacting overall development efficiency.

### The Ugly

*  Code regeneration inconsistency is a significant issue. Changing the DLL doesn't reliably trigger code regeneration, leading to situations where regeneration must be manually forced by calling assembly reload after the generator's assembly reloads.
*  The integration of code generators into multiple, if not all, assemblies presents a risk of breaking those assemblies. This risk is compounded if the generated code does not include checks to determine the assembly in which it's running.
*  Debugging is hindered by the inability to directly click through debug information when errors occur in the code, complicating the troubleshooting process.
*  Scanning for deep link attributes and correctly mapping attribute property values or constructor usage is a complex task. Especially challenging is understanding the positioning of parameters in constructors and mapping them to the correct values. Consequently, the decision was made to use properties exclusively, leading to the attribute format `[DeepLink(ViewDataType = typeof(ViewAData))]` instead of `[DeepLink(typeof(ViewAData))]`. This workaround, while effective, represents a limitation in the system's flexibility.
*  Since code generation occurs at compile time, it's not feasible to retrieve type data directly from the attribute. The workaround involved using only the type name and scanning the entire assembly to find the matching type. This approach, while not ideal, was a necessary compromise given the time constraints and the overall complexity of the other drawbacks.
*  Hardcoding namespaces, like in `UISystem.UIManager`, was sometimes necessary because adding a 'using' statement could break the generated class in assemblies lacking the `UISystem`. A potential solution might involve limiting the assemblies where the generator runs, but for now, ensuring that the generated class remains empty in such assemblies is the workaround. This approach, although functional, indicates a lack of fine-grained control over the generator's scope and application.++++