﻿using UnityEngine;

namespace Source
{
    public class Print : MonoBehaviour
    {
        private void OnEnable()
        {
            SomeClass instance = new();
            Debug.Log(instance.AsString());
        }
    }
}