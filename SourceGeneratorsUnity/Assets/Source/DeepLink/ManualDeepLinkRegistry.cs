﻿using System;
using System.Collections.Generic;
using GameUI;
using UISystem;

namespace Source.DeepLink
{
    public static class ManualDeepLinkRegistry
    {
        public static void OpenFromTypeName(UIManager manager, string typeName, Dictionary<string, string> parameters)
        {
            string lowerTypeName = typeName.ToLower();

            if (lowerTypeName == nameof(ViewA).ToLower())
            {
                OpenViewA(manager, parameters);
                return;
            }
            
            if (lowerTypeName == nameof(MainView).ToLower())
            {
                OpenMainView(manager, parameters);
                return;
            }
        }

        private static void OpenViewA(UIManager manager, IReadOnlyDictionary<string, string> parameters)
        {
            if (parameters == null || !parameters.ContainsKey("value"))
            {
                throw new Exception("Missing value for view!");
            }
            
            int value = int.Parse(parameters["value"]);
            manager.OpenView<ViewA, ViewAData>(new ViewAData(value));
        }
        
        private static void OpenMainView(UIManager manager, IReadOnlyDictionary<string, string> _)
        {
            manager.OpenView<MainView>();
        }
    }
}