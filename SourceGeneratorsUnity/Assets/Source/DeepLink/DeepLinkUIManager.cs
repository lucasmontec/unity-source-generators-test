﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UISystem;
using UnityEngine;

namespace DeepLink
{
    public class DeepLinkUIManager : SerializedMonoBehaviour
    {
        [Required]
        public UIManager manager;
        
        [Button]
        public void OpenMainView()
        {
            OpenLink($"game://mainview");
        }
        
        [Button]
        public void OpenViewA(int value)
        {
            OpenLink($"game://viewa?value={value}");
        }
        
        [Button]
        public void OpenLink(string link)
        {
            if (!TryParseURL(link, out var view, out var parameters)) return;
            
            if (string.IsNullOrEmpty(view))
            {
                return;
            }
                
            //ManualDeepLinkRegistry.OpenFromTypeName(manager, view, parameters);
            //DeepLinkRegistry.OpenViewByTypeName(manager, view, parameters);
            ZGeneratorDeepLinkRegistry.OpenViewByTypeName(manager, view, parameters);
        }
        
        public static bool TryParseURL(string url, out string view, out Dictionary<string, string> parameters)
        {
            view = string.Empty;
            parameters = new Dictionary<string, string>();
        
            if (string.IsNullOrEmpty(url))
            {
                return false;
            }

            if (!Uri.TryCreate(url, UriKind.Absolute, out Uri uri))
            {
                Debug.LogWarning("Couldn't create valid URI");
                return false;
            }

            view = uri.Host;
            var query = uri.Query;

            if (string.IsNullOrEmpty(query))
            {
                Debug.Log("Query is empty!");
                return true;
            }

            query = query.TrimStart('?');
            foreach (var param in query.Split('&'))
            {
                var parts = param.Split('=');
                if (parts.Length == 2)
                {
                    parameters.Add(parts[0], parts[1]);
                }
            }

            return true;
        }
    }
}