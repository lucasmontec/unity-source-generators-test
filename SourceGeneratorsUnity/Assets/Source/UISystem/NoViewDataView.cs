﻿namespace UISystem
{
    public abstract class NoViewDataView : BaseView<NullViewData>
    {
        public override void Setup(NullViewData data) {}
    }
}