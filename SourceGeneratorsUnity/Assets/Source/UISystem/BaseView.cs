﻿using UnityEngine;

namespace UISystem
{
    public abstract class BaseView<T> : MonoBehaviour, IView where T : BaseViewData
    {
        public abstract void Setup(T data);
        public GameObject Owner => gameObject;
    }
}