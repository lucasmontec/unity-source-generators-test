﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace UISystem
{
    public class UIManager : SerializedMonoBehaviour
    {
        [OdinSerialize, NonSerialized]
        public List<IView> Views = new();
        
        private readonly Dictionary<Type, GameObject> _viewPrefabs = new();

        public Transform viewsRoot;

        private GameObject _openedView;

        private void OnEnable()
        {
            _viewPrefabs.Clear();
            foreach (IView view in Views)
            {
                Debug.Log($"Mapping view type {view.GetType()} to object {view.Owner.name}");
                _viewPrefabs[view.GetType()] = view.Owner;
            }
        }

        public void OpenView<T, TData>(TData data) 
            where T : BaseView<TData>
            where TData : BaseViewData
        {
            Type viewType = typeof(T);
            if (!_viewPrefabs.ContainsKey(viewType))
            {
                Debug.LogWarning($"No view for type {viewType.Name}");
                return;
            }
            
            var prefab = _viewPrefabs[viewType];
            var newView = Instantiate(prefab, viewsRoot);
            var view = newView.GetComponent<T>();

            if (!view)
            {
                Destroy(newView);
                throw new Exception($"No view controller {viewType.Name} on view prefab {prefab.name}!");
            }
            
            view.Setup(data);
            
            Destroy(_openedView);
            _openedView = newView;
        }
        
        public void OpenView<T>() 
            where T : NoViewDataView
        {
            Type viewType = typeof(T);
            if (!_viewPrefabs.ContainsKey(viewType))
            {
                Debug.LogWarning($"No view for type {viewType.Name}");
                return;
            }
            
            var prefab = _viewPrefabs[viewType];
            var newView = Instantiate(prefab, viewsRoot);

            newView.GetComponent<NoViewDataView>().Setup(new NullViewData());
            
            Destroy(_openedView);
            _openedView = newView;
        }
    }
}