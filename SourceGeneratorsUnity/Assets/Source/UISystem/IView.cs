﻿using UnityEngine;

namespace UISystem
{
    public interface IView
    {
        GameObject Owner { get; }
    }
}