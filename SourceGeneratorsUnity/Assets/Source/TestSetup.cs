﻿using GameUI;
using Sirenix.OdinInspector;
using UISystem;

namespace Source
{
    public class TestSetup : SerializedMonoBehaviour
    {
        public UIManager manager;

        private void OnEnable()
        {
            OpenMainView();
        }

        [Button]
        public void OpenMainView()
        {
            manager.OpenView<MainView>();
        }

        [Button]
        public void OpenViewA()
        {
            manager.OpenView<ViewA, ViewAData>(new ViewAData(10));
        }
    }
}