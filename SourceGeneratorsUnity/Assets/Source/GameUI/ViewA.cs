﻿using TMPro;
using UISystem;

namespace GameUI
{
    public class ViewAData : BaseViewData
    {
        public readonly int Value;

        public ViewAData(int value)
        {
            Value = value;
        }
    }
    
    [DeepLink(ViewDataType = typeof(ViewAData))]
    public class ViewA : BaseView<ViewAData>
    {
        public TextMeshProUGUI valueText;
        
        public override void Setup(ViewAData data)
        {
            valueText.text = $"Value: {data.Value}";
        }
    }
}