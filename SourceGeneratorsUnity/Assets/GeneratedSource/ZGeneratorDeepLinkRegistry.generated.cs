/* GENERATED SOURCE FILE - DON'T CHANGE MANUALLY */

using UnityEngine;
using System;
using System.Collections.Generic;
public partial class ZGeneratorDeepLinkRegistry 
{
	protected static Dictionary<string, OpenViewAction> _openViewActions = new()
	{
		[nameof(GameUI.MainView).ToLower()] = OpenMainView,
		[nameof(GameUI.ViewA).ToLower()] = OpenViewA,
	};
	protected delegate void OpenViewAction(UISystem.UIManager manager, IReadOnlyDictionary<string, string> parameters);
	public static void OpenViewByTypeName(UISystem.UIManager manager, string viewTypeName, IReadOnlyDictionary<string, string> parameters)
	{
	    if (!_openViewActions.ContainsKey(viewTypeName))
	    {
	        throw new Exception($"No view type deeplink registered for: {viewTypeName}.");
	    }
	    _openViewActions[viewTypeName](manager, parameters);
	}
	public static void OpenMainView(UISystem.UIManager manager, IReadOnlyDictionary<string, string> _)
	{
		manager.OpenView<GameUI.MainView>();
	}
	public static void OpenViewA(UISystem.UIManager manager, IReadOnlyDictionary<string, string> parameters)
	{
		var value = int.Parse(parameters["value"]);
		manager.OpenView<GameUI.ViewA, GameUI.ViewAData>(new GameUI.ViewAData(value));
	}
};