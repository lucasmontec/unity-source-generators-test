﻿using SatorImaging.UnitySourceGenerator;

namespace UnityAltSourceGen
{
    [UnitySourceGenerator(typeof(TestGenerator), OverwriteIfFileExists = true)]
    public partial class TestTargetClass
    {
    }
}