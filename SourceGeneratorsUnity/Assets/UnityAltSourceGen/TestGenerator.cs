﻿using System.Text;
using SatorImaging.UnitySourceGenerator;

namespace UnityAltSourceGen
{
    public class TestGenerator
    {
        private static string OutputFileName() => "PanicMethod.cs";  // -> PanicMethod.<TargetClass>.<GeneratorClass>.g.cs

        private static bool Emit(USGContext context, StringBuilder sb)
        {
            if (!context.TargetClass.IsClass || context.TargetClass.IsAbstract)
                return false;  // return false to tell USG doesn't write file.

            // code generation
            sb.Append($@"
            namespace {context.TargetClass.Namespace}
            {{
                //PUBLIC
                public partial class {context.TargetClass.Name}
                {{
                    public void Panic() => throw new System.Exception();
                }}
            }}
            ");
            return true;
        }
    }
}