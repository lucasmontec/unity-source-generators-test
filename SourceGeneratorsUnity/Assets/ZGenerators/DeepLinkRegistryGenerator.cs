﻿using System;
using System.Linq;
using System.Text;
using DeepLink;
using Sirenix.OdinInspector;
using UnityEngine;
using ZSourceGenerator;
using ZSourceGenerator.CodeGeneration;

namespace ZGenerators
{
    [CreateAssetMenu(menuName = "Scriptable Generator")]
    public class DeepLinkRegistryGenerator : ScriptableSourceGenerator
    {
        [Required]
        public TextAsset sourceTemplate;
        
        private const string DictionaryMappingTemplateKey = "%DICTIONARY_VIEW_METHOD_MAPPING%";
        private const string ViewMethodsListTemplateKey = "%VIEW_METHODS%";
        
        public override string GeneratedFileName => nameof(ZGeneratorDeepLinkRegistry);
        
        public override string GenerateSource(GenerationContext context)
        {
            var typesAndAttributes = 
                AssemblyScanExtensions.AllAssemblies.GetAllTypesWithAttribute<DeepLinkAttribute>();

            StringBuilder viewNameToMethodMappings = new();
            StringBuilder viewMethods = new();
            foreach (TypeWithAttribute<DeepLinkAttribute> typeAndAttribute in typesAndAttributes)
            {
                //Mapping string:
                //[nameof(GameUI.ViewA).ToLower()] = OpenViewA,
                Type viewType = typeAndAttribute.Type;
                
                var qualifiedViewName = viewType.FullName;
                var viewName = viewType.Name;
                viewNameToMethodMappings.AppendLine($"\t[nameof({qualifiedViewName}).ToLower()] = Open{viewName},");

                Type viewDataType = typeAndAttribute.Attribute.ViewDataType;
                viewMethods.AppendLine(viewDataType is null
                    ? CreateSimpleViewMethod(viewName, qualifiedViewName)
                    : CreateViewWithViewDataMethod(viewName, qualifiedViewName, viewDataType));
            }

            string templateSource = sourceTemplate.text;

            var source = templateSource
                .Replace(DictionaryMappingTemplateKey, viewNameToMethodMappings.ToString());
            
            source = source.Replace(ViewMethodsListTemplateKey, viewMethods.ToString());
            
            var deepLinkClass = CodeGen.WrapWithClass(
                source, 
                nameof(ZGeneratorDeepLinkRegistry), 
                visibilityModifier: CodeGen.VisibilityModifier.Public, 
                partial: true);
            
            var sourceCode = CodeGen.AddUsingUnityEngineAndSystem(CodeGen.AddUsingGenericCollections(deepLinkClass));
            return CodeGen.RemoveEmptyLines(sourceCode);
        }

        private static string CreateSimpleViewMethod(string viewName, string qualifiedViewName)
        {
            return CodeGen.CreateMethod(
                $"Open{viewName}", 
                "UISystem.UIManager manager, IReadOnlyDictionary<string, string> _", 
                $"manager.OpenView<{qualifiedViewName}>();", isStatic: true);
        }

        private static string CreateViewWithViewDataMethod(string viewName, string qualifiedViewName, Type viewDataType)
        {
            var parameters = new StringBuilder();
            var constructorArgs = new StringBuilder();
            var constructor = viewDataType.GetConstructors().FirstOrDefault();

            if (constructor != null)
            {
                foreach (var param in constructor.GetParameters())
                {
                    string paramName = param.Name;
                    string paramType = param.ParameterType.Name;
                    parameters.AppendLine($"var {paramName} = {ParseParameter(paramType)}(parameters[\"{paramName}\"]);");
                    constructorArgs.Append($"{paramName}, ");
                }

                constructorArgs.Length -= 2; // Remove the last comma and space
            }

            return CodeGen.CreateMethod(
                $"Open{viewName}",
                "UISystem.UIManager manager, IReadOnlyDictionary<string, string> parameters",
                $"{parameters}\nmanager.OpenView<{qualifiedViewName}, {viewDataType.FullName}>(new {viewDataType.FullName}({constructorArgs}));",
                isStatic: true
            );
        }
        
        private static string ParseParameter(string paramType)
        {
            return paramType switch
            {
                "Int32" => "int.Parse",
                "String" => "" // Strings don't need parsing
                ,
                _ => throw new NotImplementedException($"Parser not implemented for type {paramType}")
            };
        }
    }
}