﻿using System.Collections.Generic;
using Commands;

public interface IGenerationPostProcessor
{
    public void Process(List<GenerationCommand> commands);
}