﻿namespace Commands
{
    public readonly struct GenerationCommand
    {
        public string Directory { get; }
        public string FilePath { get; }
        public string SourceCode { get; }

        public GenerationCommand(string directory, string filePath, string sourceCode)
        {
            FilePath = filePath;
            SourceCode = sourceCode;
            Directory = directory;
        }
    }
}