﻿using GeneratedFilesHandling;
using Settings;
using SourceExtension;

namespace EditorInstances
{
    public static class EditorZGenerator
    {
        private static ZGenerator editorGenerator;
    
        public static ZGenerator Generator => editorGenerator ??= CreateGenerator();

        private static ZGenerator CreateGenerator()
        { 
            var scanner = new AssemblyGeneratorScanner();
            GeneratorSettings generatorSettings = GeneratorSettingsEditorPrefsPersistence.LoadSettings();
        
            var generator = new ZGenerator(scanner, generatorSettings);
            generator.EnableSourceExtensionForAllAssemblies();
            generator.EnableCachingOfGeneratedSourceFiles();
            return generator;
        }

        public static ZGenerator RefreshGenerator()
        {
            return editorGenerator = CreateGenerator();
        }
    }
}