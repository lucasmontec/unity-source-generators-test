﻿using System;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using EditorInstances;
using Settings;
using ZSourceGenerator;

namespace UI
{
    public class ZGeneratorWindow : EditorWindow, IHasCustomMenu
    {
        private ZGenerator _zGenerator;
        private IGeneratorScanner _scanner;
        private readonly List<ISourceGenerator> _generators = new();
        private Vector2 _scrollPos;

        public static ZGeneratorWindow CurrentWindow;
        
        [MenuItem("Tools/ZSource Generation/Run All Generators", priority = 0)]
        public static void GenerateAllButton()
        {
            DirectGenerationCommands.RunAllGeneratorsAndLogGenerationTime();
        }
        
        [MenuItem("Tools/ZSource Generation/ZGenerators Manager Window")]
        public static void ShowWindow()
        {
            GUIContent titleContent = new(EditorGUIUtility.IconContent("d_align_horizontally_right_active"))
            {
                text = "ZGenerators Manager"
            };
            
            var window = GetWindow<ZGeneratorWindow>();
            window.titleContent = titleContent;

            CurrentWindow = window;
        }

        private void OnEnable()
        {
            _scanner = new AssemblyGeneratorScanner();
            _zGenerator = EditorZGenerator.RefreshGenerator();
            
            ScanGenerators();

            ZGeneratorSettingsProvider.OnSettingsChanged += RefreshGeneratorAndScan;
            AssemblyReloadEvents.afterAssemblyReload += RefreshGeneratorAndScan;

        }

        private void OnDisable()
        {
            ZGeneratorSettingsProvider.OnSettingsChanged -= RefreshGeneratorAndScan;
            AssemblyReloadEvents.afterAssemblyReload -= RefreshGeneratorAndScan;
        }

        public void RefreshGeneratorAndScan()
        {
            _zGenerator = EditorZGenerator.RefreshGenerator();
            ScanGenerators();
        }

        public void AddItemsToMenu(GenericMenu menu)
        {
            menu.AddItem(new GUIContent("Open Generator Settings"),
                false, ZGeneratorSettingsProvider.OpenGeneratorSettings);
        }

        private void OnGUI()
        {
            GUILayout.Space(5);
            RenderGeneratorsList();
            
            GUILayout.Space(10);
            GenerateAllSourcesButton();
        }
        
        private void GenerateAllSourcesButton()
        {
            if (!GUILayout.Button("Generate All Sources")) return;
            DirectGenerationCommands.RunAllGeneratorsAndLogGenerationTime(_zGenerator);
        }

        private void ScanGenerators()
        {
            _generators.Clear();
            _generators.AddRange(_scanner.ScanGenerators(AppDomain.CurrentDomain.GetAssemblies()));
        }

        private void RenderGeneratorsList()
        {
            GUILayout.Label("Generators", EditorStyles.boldLabel);
            
            GUILayout.Space(5);
            
            GUILayout.BeginVertical(GUI.skin.box);
            _scrollPos = GUILayout.BeginScrollView(_scrollPos, GUILayout.ExpandHeight(true));

            foreach (var generator in _generators)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label(generator.GetType().Name, EditorStyles.label);
        
                GUIContent generateButtonContent = new(EditorGUIUtility.IconContent("PlayButton"))
                {
                    tooltip = "Generate"
                };

                if (GUILayout.Button(generateButtonContent, GUILayout.MaxWidth(30))) 
                {
                    _zGenerator.RunSingleGenerator(generator);
                }
                GUILayout.EndHorizontal();
                GUILayout.Space(5);
            }

            GUILayout.EndScrollView();
            GUILayout.EndVertical();
        }
    }
}