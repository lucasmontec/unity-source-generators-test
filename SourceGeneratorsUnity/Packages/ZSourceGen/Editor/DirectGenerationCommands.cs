﻿using System;
using System.Diagnostics;
using EditorInstances;
using Debug = UnityEngine.Debug;

public static class DirectGenerationCommands
{
        public static void RunAllGeneratorsAndLogGenerationTime()
        {
                var generator = EditorZGenerator.RefreshGenerator();
                RunAllGeneratorsAndLogGenerationTime(generator);
        }
        
        public static void RunAllGeneratorsAndLogGenerationTime(ZGenerator generator)
        {
                Stopwatch stopwatch = new();
                stopwatch.Start();
            
                generator.GenerateAllSources();
            
                stopwatch.Stop();
                TimeSpan timeTaken = stopwatch.Elapsed;
                string timeTakenStr = timeTaken.ToString(@"m\:ss\.fff");
                Debug.Log($"All sources generated in {timeTakenStr}.");
        }
}