﻿using System.Collections.Generic;
using System.Reflection;
using ZSourceGenerator;

public interface IGeneratorScanner
{
    IEnumerable<ISourceGenerator> ScanGenerators(Assembly[] assemblies);
}