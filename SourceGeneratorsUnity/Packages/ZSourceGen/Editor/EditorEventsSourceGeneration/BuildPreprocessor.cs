﻿using Settings;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using Debug = UnityEngine.Debug;

namespace EditorEventsSourceGeneration
{
    public class BuildPreprocessor : IPreprocessBuildWithReport
    {
        public int callbackOrder => 0;
        
        public void OnPreprocessBuild(BuildReport report)
        {
            var settings = GeneratorSettingsEditorPrefsPersistence.LoadSettings();

            if (!settings.RunAllGeneratorsBeforeBuild)
            {
                return;
            }
            
            Debug.Log("Generating all sources before build.");

            DirectGenerationCommands.RunAllGeneratorsAndLogGenerationTime();
        }
    }
}