﻿using EditorInstances;

namespace GeneratedFilesHandling
{
    public static class ZGeneratorExtensions
    {
        public static ZGenerator EnableCachingOfGeneratedSourceFiles(this ZGenerator generator)
        {
            generator.RegisterGenerationPostProcessor(EditorSourceFilesCache.Cache);
            return generator;
        }
    }
}