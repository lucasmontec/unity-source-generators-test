﻿using System.Collections.Generic;

namespace Util
{
    public static class HashSetExtensions
    {
        public static HashSet<T> AddRange<T>(this HashSet<T> set, IEnumerable<T> range)
        {
            foreach (T value in range)
            {
                set.Add(value);
            }

            return set;
        }
    }
}