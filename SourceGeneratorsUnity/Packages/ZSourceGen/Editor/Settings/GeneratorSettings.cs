﻿using System;
using FileHandling;

namespace Settings
{
    public class GeneratorSettings
    {
        public bool RunAllGeneratorsBeforeEnteringPlayMode { get; set; }
        public bool RunAllGeneratorsBeforeBuild { get; set; } = true;
        public string GeneratedFilesHeader { get; set; } = "/* GENERATED SOURCE FILE - DON'T CHANGE MANUALLY */\n\n";
        public string GeneratedExtension { get; set; } = ".generated.cs";
        public string DefaultGeneratorOutputDirectory { get; set; } = "Assets/GeneratedSource";
        public ISourceFileWriter FileWriter { get; private set; } = new UnitySourceFileWriter();

        public void SetFileWriter(ISourceFileWriter writer)
        {
            FileWriter = writer ?? throw new ArgumentNullException(nameof(writer), "Writer cant be null!");
        }
    }
}