﻿using System;
using System.Text;

namespace ZSourceGenerator.CodeGeneration
{
    /// <summary>
    /// Code generation utility class.
    /// </summary>
    public static class CodeGen
    {
        public enum VisibilityModifier
        {
            Public,
            Private,
            Internal,
            Protected
        }

        private static string GetVisibilityModifierSource(VisibilityModifier modifier)
        {
            return modifier.ToString().ToLower();
        }
        
        /// <summary>
        /// Appends the using statement for UnityEngine and System to the top of the source.
        /// </summary>
        public static string AddUsingUnityEngineAndSystem(string source)
        {
            return $"using {nameof(UnityEngine)};\n" +
                   $"using {nameof(System)};\n\n"+
                   source;
        }
        
        public static string AddUsingGenericCollections(string source)
        {
            return $"using System.Collections.Generic;\n" +
                   source;
        }

        public static string WrapWithClass(string source, string className, Type parentType=null, VisibilityModifier visibilityModifier=VisibilityModifier.Public, bool partial=false)
        {
            var visibilityModifierSource = GetVisibilityModifierSource(visibilityModifier);
            var inheritanceCode = parentType != null ? $": {parentType.Name}" : string.Empty;
            var partialSource = partial ? "partial" : "";
            return $"{visibilityModifierSource} {partialSource} class {className} {inheritanceCode}\n{{\n{AddIndentationLevel(source)}\n}};\n";
        }
        
        public static string AppendClass(string source, string className, Type parentType=null, VisibilityModifier visibilityModifier=VisibilityModifier.Public, bool partial=false)
        {
            return source + WrapWithClass(string.Empty, className, parentType, visibilityModifier, partial);
        }
        
        /// <summary>
        /// Creates a method's code.
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="parametersSource"></param>
        /// <param name="methodSourceCode">Indented automatically.</param>
        /// <param name="returnType"></param>
        /// <param name="visibilityModifier"></param>
        /// <param name="isStatic"></param>
        /// <returns></returns>
        public static string CreateMethod(string methodName, string parametersSource, string methodSourceCode, string returnType=null, VisibilityModifier visibilityModifier=VisibilityModifier.Public, bool isStatic=false)
        {
            var visibilityModifierSource = GetVisibilityModifierSource(visibilityModifier);
            var staticSource = isStatic ? " static" : string.Empty;
            var returnTypeSource = string.IsNullOrEmpty(returnType) ? "void" : returnType;
            return $"{visibilityModifierSource}{staticSource} {returnTypeSource} {methodName}({parametersSource})\n{{\n{AddIndentationLevel(methodSourceCode)}\n}}\n";
        }
        
        public static string AppendMonoBehaviorClass(string source, string className, VisibilityModifier visibilityModifier=VisibilityModifier.Public, bool partial=false)
        {
            var visibilityModifierSource = GetVisibilityModifierSource(visibilityModifier);
            var partialSource = partial ? "partial" : "";
            return $"{visibilityModifierSource} {partialSource} class {className} : MonoBehaviour\n    {{\n    }};\n" +
                   source;
        }
        
        public static string WrapWithTargetNamespace(string source, Type targetType)
        {
            var targetNamespace = targetType.Namespace;
            return $"namespace {targetNamespace}\n{{\n" +
                   $"{AddIndentationLevel(source)}\n" +
                   $"}}";
        }
        
        public static string RemoveEmptyLines(string input)
        {
            var lines = input.Split('\n');
            var nonEmptyLines = new StringBuilder();

            foreach (var line in lines)
            {
                if (string.IsNullOrWhiteSpace(line)) continue;
                nonEmptyLines.AppendLine(line);
            }
            
            return nonEmptyLines.ToString().TrimEnd('\n');
        }
        
        /// <summary>
        /// Adds an indentation level to all lines.
        /// </summary>
        public static string AddIndentationLevel(string input)
        {
            var lines = input.Split('\n');
            var indentedText = new StringBuilder();

            foreach (var line in lines)
            {
                var trimmedLine = line.TrimEnd();

                if (!string.IsNullOrWhiteSpace(trimmedLine))
                {
                    indentedText.Append("\t" + trimmedLine);
                }
                else
                {
                    indentedText.Append(trimmedLine);
                }
                indentedText.Append('\n');
            }

            return indentedText.ToString().TrimEnd('\n');
        }

    }
}