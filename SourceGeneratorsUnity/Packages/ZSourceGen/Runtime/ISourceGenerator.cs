﻿namespace ZSourceGenerator
{
    public interface ISourceGenerator
    {
        /// <summary>
        /// File name with no extensions.
        /// The generated files will have a '.generated.cs' extension
        /// </summary>
        public string GeneratedFileName { get; }

        /// <summary>
        /// If set, the generated sources will be placed under this directory.
        /// This path must be relative to the Assets/ directory.
        /// </summary>
        public string SourceDirectory { get; }
        
        /// <summary>
        /// This is the method that creates the code.
        /// </summary>
        /// <returns>The code to be created.</returns>
        public string GenerateSource(GenerationContext context);

        /// <summary>
        /// Return false if the generator should be skipped.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool ShouldGenerate(GenerationContext context)
        {
            return true;
        }
    }
}