﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ZSourceGenerator.SourceExtension
{
    public abstract class SourceExtensionGenerator : ISourceGenerator
    {
        private static readonly Dictionary<Type, string> typeToScriptPathCache = new();
        
#if UNITY_EDITOR
        static SourceExtensionGenerator()
        {
            CacheMonoScriptPaths();
        }
#endif

        public abstract bool ShouldGenerate(GenerationContext context);

        public string GeneratedFileName { get; private set; }
        public string SourceDirectory { get; private set; }
    
        public string GenerateSource(GenerationContext context)
        {
            Type targetType = context.CurrentTarget;
        
            GeneratedFileName = targetType.Name;
        
            if (typeToScriptPathCache.TryGetValue(targetType, out var path))
            {
                SourceDirectory = path.TrimStart("Assets/".ToCharArray());
            }
            else
            {
                throw new InvalidOperationException($"No script file found for type {targetType.Name}");
            }

            return ExtendSource(context);
        }

        protected abstract string ExtendSource(GenerationContext context);
    
#if UNITY_EDITOR
        private static void CacheMonoScriptPaths()
        {
            foreach (var monoScript in Resources.FindObjectsOfTypeAll<MonoScript>())
            {
                var type = monoScript.GetClass();
                if (type == null) continue;
            
                string path = AssetDatabase.GetAssetPath(monoScript);
                typeToScriptPathCache[type] = Path.GetDirectoryName(path);
            }
        }
#endif
    }
}