﻿using UnityEngine;

namespace ZSourceGenerator
{
    public abstract class ScriptableSourceGenerator : 
        ScriptableObject,
        ISourceGenerator
    {
        public virtual string GeneratedFileName => null;
        public virtual string SourceDirectory => null;
    
        public abstract string GenerateSource(GenerationContext context);
    }
}