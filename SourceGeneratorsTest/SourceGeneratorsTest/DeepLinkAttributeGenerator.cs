﻿using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;
using SourceGeneratorsTest.obj;

namespace SourceGeneratorsTest;

[Generator]
public class DeepLinkAttributeGenerator : ISourceGenerator
{
    public void Initialize(GeneratorInitializationContext context) { }

    public void Execute(GeneratorExecutionContext context)
    {
        const string attributeSource = $$"""
            using System;

            [AttributeUsage(AttributeTargets.Class, Inherited = false)]
            internal class {{DeepLinkAttributeInfo.AttributeClassName}} : Attribute
            {
                public Type ViewDataType { get; set; }
            }
        """;

        context.AddSource("DeepLinkAttribute.cs", SourceText.From(attributeSource, Encoding.UTF8));
    }
}
