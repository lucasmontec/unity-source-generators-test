﻿using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;

namespace SourceGeneratorsTest;

[Generator]
public class AsStringAttributeGenerator : ISourceGenerator
{
    public void Execute(GeneratorExecutionContext context)
    {
        // Generate the AsStringAttribute
        const string attributeSource = """
                                       
                                                   using System;
                                                   [AttributeUsage(AttributeTargets.Class)]
                                                   internal class AsStringAttribute : Attribute { }
                                               
                                       """;
        context.AddSource("AsStringAttribute.cs", SourceText.From(attributeSource, Encoding.UTF8));
    }

    public void Initialize(GeneratorInitializationContext context) { }
}