﻿using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using SourceGeneratorsTest.obj;

namespace SourceGeneratorsTest;

public class DeepLinkAttributeSyntaxReceiver : ISyntaxReceiver
{
    public List<AttributeDefinition> AttributeDefinitions { get; } = new();
    
    public void OnVisitSyntaxNode(SyntaxNode syntaxNode)
    {
        if (syntaxNode is not AttributeSyntax attributeSyntax) return;
        
        var collector = new AttributeCollector(
            DeepLinkAttributeInfo.AttributeName, 
            DeepLinkAttributeInfo.AttributeClassName);
            
        attributeSyntax.Accept(collector);
        AttributeDefinitions.AddRange(collector.AttributeDefinitions);
    }
}

internal class AttributeCollector : CSharpSyntaxVisitor
{
    private readonly HashSet<string> _attributeNames;

    public List<AttributeDefinition> AttributeDefinitions { get; } = new();

    public AttributeCollector(params string[] attributeNames)
    {
        _attributeNames = new HashSet<string>(attributeNames);
    }

    public override void VisitAttribute(AttributeSyntax node)
    {
        base.VisitAttribute(node);

        if (!_attributeNames.Contains(node.Name.ToString()))
        {
            return;
        }

        var fieldArguments = new List<(string Name, object Value)>();
        var propertyArguments = new List<(string Name, object Value)>();

        var arguments = node.ArgumentList?.Arguments.ToArray() ?? Array.Empty<AttributeArgumentSyntax>();
        foreach (var syntax in arguments)
        {
            if (syntax.NameColon != null)
            {
                fieldArguments.Add((syntax.NameColon.Name.ToString(), syntax.Expression));
            }
            else if (syntax.NameEquals != null)
            {
                propertyArguments.Add((syntax.NameEquals.Name.ToString(), syntax.Expression));
            }
            else
            {
                fieldArguments.Add((string.Empty, syntax.Expression));
            }
        }
        
        var classInfo = FindEnclosingClass(node);

        AttributeDefinitions.Add(new AttributeDefinition
        {
            Name = node.Name.ToString(),
            FieldArguments = fieldArguments.ToArray(),
            PropertyArguments = propertyArguments.ToArray(),
            EnclosingClass = classInfo!
        });
    }
    
    private static ClassDeclarationSyntax? FindEnclosingClass(SyntaxNode node)
    {
        while (node != null)
        {
            if (node is ClassDeclarationSyntax classDecl)
            {
                return classDecl;
            }
            node = node.Parent;
        }
        return null;
    }
}

public record AttributeDefinition
{
    public string? Name { get; set; }
    public (string Name, object Value)[] FieldArguments { get; set; } = Array.Empty<(string Name, object Value)>();
    public (string Name, object Value)[] PropertyArguments { get; set; } = Array.Empty<(string Name, object Value)>();

    public ClassDeclarationSyntax EnclosingClass { get; set; }
    
    public string ToSource()
    {
        var definition = new StringBuilder(Name);
        if (!FieldArguments.Any() && !PropertyArguments.Any())
        {
            return definition.ToString();
        }

        return definition
            .Append("(")
            .Append(ArgumentsToString())
            .Append(")")
            .ToString();
    }

    private string ArgumentsToString()
    {
        var arguments = new StringBuilder();

        if (FieldArguments.Any())
        {
            arguments.Append(string.Join(", ", FieldArguments.Select(
                param => string.IsNullOrEmpty(param.Name)
                    ? $"{param.Value}"
                    : $"{param.Name}: {param.Value}")
            ));
        }

        if (PropertyArguments.Any())
        {
            arguments
                .Append(arguments.Length > 0 ? ", " : "")
                .Append(string.Join(", ", PropertyArguments.Select(
                    param => $"{param.Name} = {param.Value}")
                ));
        }

        return arguments.ToString();
    }
}