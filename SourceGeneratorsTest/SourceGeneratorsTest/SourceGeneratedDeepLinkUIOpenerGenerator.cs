﻿using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;
using SourceGeneratorsTest.obj;

namespace SourceGeneratorsTest;

[Generator]
public class SourceGeneratedDeepLinkUiOpenerGenerator : ISourceGenerator
{
    private const string LOG_FILE_PATH = "GeneratorLog.txt";

    public void Initialize(GeneratorInitializationContext context)
    {
        context.RegisterForSyntaxNotifications(() => new DeepLinkAttributeSyntaxReceiver());
    }

    public void Execute(GeneratorExecutionContext context)
    {
        File.AppendAllText(LOG_FILE_PATH, $"\nNew generation {DateTime.Now}: ");
        try
        {
            ExecuteGeneration(context);
        }
        catch (Exception e)
        {
            File.AppendAllText(LOG_FILE_PATH, $"Source generator error:\n{e}\n");
        }
    }

    private static string GetClassName(ClassDeclarationSyntax classDecl)
    {
        return classDecl.Identifier.Text;
    }

    private static string GetFullyQualifiedName(INamedTypeSymbol typeSymbol)
    {
        if (typeSymbol == null)
        {
            throw new ArgumentNullException(nameof(typeSymbol));
        }

        var names = new Stack<string>();
        INamedTypeSymbol currentSymbol = typeSymbol;

        while (currentSymbol != null)
        {
            names.Push(currentSymbol.Name);
            currentSymbol = currentSymbol.ContainingType;
        }

        var namespaceSymbol = typeSymbol.ContainingNamespace;
        while (namespaceSymbol != null && !namespaceSymbol.IsGlobalNamespace)
        {
            names.Push(namespaceSymbol.Name);
            namespaceSymbol = namespaceSymbol.ContainingNamespace;
        }

        return string.Join(".", names);
    }
    
    private static string GetFullyQualifiedName(SyntaxNode? node)
    {
        var names = new Stack<string>();
        while (node != null)
        {
            switch (node)
            {
                case ClassDeclarationSyntax classDecl:
                    names.Push(classDecl.Identifier.Text);
                    break;
                case NamespaceDeclarationSyntax namespaceDecl:
                    names.Push(namespaceDecl.Name.ToString());
                    break;
            }
            node = node.Parent;
        }
        return string.Join(".", names);
    }

    private static string RemoveTypeof(string input)
    {
        const string prefix = "typeof(";
        const string suffix = ")";

        if (input.StartsWith(prefix) && input.EndsWith(suffix))
        {
            return input.Substring(prefix.Length, input.Length - prefix.Length - suffix.Length);
        }

        return input;
    }

    private static INamedTypeSymbol? FindClassSymbol(Compilation compilation, string className)
    {
        var viewDataSymbol = FindInNamespace(compilation.GlobalNamespace);

        if (viewDataSymbol != null) return viewDataSymbol;
        
        foreach (var reference in compilation.References)
        {
            if (compilation.GetAssemblyOrModuleSymbol(reference) is not IAssemblySymbol assemblySymbol) continue;
            
            viewDataSymbol = FindInNamespace(assemblySymbol.GlobalNamespace);
            if (viewDataSymbol != null)
            {
                break;
            }
        }

        return viewDataSymbol;

        INamedTypeSymbol? FindInNamespace(INamespaceSymbol namespaceSymbol)
        {
            foreach (var typeMember in namespaceSymbol.GetTypeMembers())
            {
                if (typeMember.Name == className)
                {
                    return typeMember;
                }
            }

            foreach (var nestedNamespace in namespaceSymbol.GetNamespaceMembers())
            {
                var result = FindInNamespace(nestedNamespace);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }
    }


    
    private static void ExecuteGeneration(GeneratorExecutionContext context)
    {
        if (context.SyntaxReceiver is not DeepLinkAttributeSyntaxReceiver attributeSyntaxReceiver)
        {
            File.AppendAllText(LOG_FILE_PATH, "context.SyntaxReceiver is not DeepLinkAttributeSyntaxReceiver!");
            return;
        }
        
        StringBuilder methodsBuilder = new("");
        List<(string simpleName,string fullName)> viewNames = new();
        
        foreach (var attributeDefinition in attributeSyntaxReceiver.AttributeDefinitions)
        {
            string viewClassName = GetClassName(attributeDefinition.EnclosingClass);
            string fullyQualifiedViewClassName = GetFullyQualifiedName(attributeDefinition.EnclosingClass);
            
            File.AppendAllText(LOG_FILE_PATH, $"\nEnclosing Class Name: {viewClassName}" +
                                              $"\nfull name: {fullyQualifiedViewClassName}" +
                                              $"\nEnclosing Class Code: {attributeDefinition.EnclosingClass}");

            string viewDataType = string.Empty;
            
            foreach((string name, object value) in attributeDefinition.PropertyArguments)
            {
                File.AppendAllText(LOG_FILE_PATH, $"\nName: {name}, Value: {value}");
                if (name == "ViewDataType")
                {
                    viewDataType = RemoveTypeof(value.ToString());
                }
            }
            
            File.AppendAllText(LOG_FILE_PATH, "\n\n");

            bool emptyDataType = string.IsNullOrEmpty(viewDataType);
            
            string methodSource =
                emptyDataType
                    ? GenerateOpenViewNoDataMethodSource(viewClassName, fullyQualifiedViewClassName)
                    : GenerateOpenViewMethodSource(context, viewClassName, fullyQualifiedViewClassName, viewDataType);

            viewNames.Add((viewClassName, fullyQualifiedViewClassName));
            
            methodsBuilder.AppendLine(methodSource);
            File.AppendAllText(LOG_FILE_PATH, $"\nEmpty data type? {emptyDataType}\nMethod Appended: {methodSource}");
        }

        var mapping = GenerateViewActionsDictionary(viewNames);
        
        var registryClassSource = GenerateDeepLinkRegistryClass(mapping, methodsBuilder.ToString());
        context.AddSource("DeepLinkRegistry_Generated.cs", SourceText.From(registryClassSource, Encoding.UTF8));
    }

    private static string GenerateViewActionsDictionary(IList<(string simpleName, string fullName)> viewNames)
    {
        if (!viewNames.Any())
        {
            return "";
        }
        
        StringBuilder viewMappings = new();

        foreach ((string simpleName, string fullName) in viewNames)
        {
            viewMappings.AppendLine($"\t[nameof({fullName}).ToLower()] = Open{simpleName},");
        }
        
        return $$"""
                 
                 protected static Dictionary<string, OpenViewAction> _openViewActions = new()
                 {
                     {{viewMappings}}
                 };
                 
                 protected delegate void OpenViewAction(UISystem.UIManager manager, IReadOnlyDictionary<string, string> parameters);
                 
                 public static void OpenViewByTypeName(UISystem.UIManager manager, string viewTypeName, IReadOnlyDictionary<string, string> parameters)
                 {
                     if (!_openViewActions.ContainsKey(viewTypeName))
                     {
                         throw new Exception($"No view type deeplink registered for: {viewTypeName}.");
                     }
                 
                     _openViewActions[viewTypeName](manager, parameters);
                 }
                 """;
    }
    
    private static string GenerateOpenViewNoDataMethodSource(string className, string fullyQualifiedClassName)
    {
        return $$"""
                     public static void Open{{className}}(UISystem.UIManager manager, IReadOnlyDictionary<string, string> _)
                     {
                         manager.OpenView<{{fullyQualifiedClassName}}>();
                     }
                 """;
    }

    private static string GenerateOpenViewMethodSource(GeneratorExecutionContext context, string className, string fullyQualifiedClassName, string viewDataType)
    {
        var viewDataSymbol = FindClassSymbol(context.Compilation, viewDataType);
        if (viewDataSymbol == null)
        {
            File.AppendAllText(LOG_FILE_PATH, $"\nFailed to find viewDataType symbol {viewDataType}!");
            return string.Empty;
        }
        
        var viewDataConstructor = viewDataSymbol.Constructors.FirstOrDefault();
        if (viewDataConstructor == null)
        {
            File.AppendAllText(LOG_FILE_PATH, $"\nNo viewDataConstructor! for {className}. viewDataType {viewDataType} View data symbol: {viewDataSymbol}");
            return string.Empty;
        }

        var fullViewDataName = GetFullyQualifiedName(viewDataSymbol);

        var parameters = viewDataConstructor.Parameters;
        var paramCreationCode = new StringBuilder();
        
        foreach (var parameterSymbol in parameters)
        {
            string parseExpression = parameterSymbol.Type.SpecialType == SpecialType.System_Object 
                ? 
                "parameters[\"{parameterSymbol.Name}\"].ToString()"
                : 
                $"{GetParseExpression(parameterSymbol)}(parameters[\"{parameterSymbol.Name}\"])";

            paramCreationCode.AppendLine($"var {parameterSymbol.Name} = {parseExpression};");
        }

        var viewDataConstructorCode = $"new {fullViewDataName}({string.Join(", ", parameters.Select(p => p.Name))})";
        
        return $$"""
        
            public static void Open{{className}}(UISystem.UIManager manager, IReadOnlyDictionary<string, string> parameters)
            {
                {{paramCreationCode}}
                manager.OpenView<{{fullyQualifiedClassName}}, {{fullViewDataName}}>({{viewDataConstructorCode}});
            }
            
        """;
    }
    
    private static string GetParseExpression(IParameterSymbol param)
    {
        return 
            param.Type.TypeKind == TypeKind.Enum ? $"Enum.Parse(typeof({param.Type}),"
                :
                param.Type.SpecialType switch
        {
            SpecialType.System_Boolean => "bool.Parse",
            SpecialType.System_Byte => "byte.Parse",
            SpecialType.System_Char => "char.Parse",
            SpecialType.System_Decimal => "decimal.Parse",
            SpecialType.System_Double => "double.Parse",
            SpecialType.System_Int16 => "short.Parse",
            SpecialType.System_Int32 => "int.Parse",
            SpecialType.System_Int64 => "long.Parse",
            SpecialType.System_Single => "float.Parse",
            SpecialType.System_UInt16 => "ushort.Parse",
            SpecialType.System_UInt32 => "uint.Parse",
            SpecialType.System_UInt64 => "ulong.Parse",
            SpecialType.System_DateTime => "DateTime.Parse",
            SpecialType.System_String => "", // Strings don't need parsing
            _ => throw new Exception($"Unsupported parameter type: {param.Type}")
        };
    }

    private static string GenerateDeepLinkRegistryClass(string mapping, string methods)
    {
        return $$"""
                 using System.Collections.Generic;
                 using System;
                 
                 public partial class DeepLinkRegistry
                 {
                    {{mapping}}
                    {{methods}}
                 }
                 """;
    }
}