﻿namespace SourceGeneratorsTest.obj;

public static class DeepLinkAttributeInfo
{
    public const string AttributeClassName = "DeepLinkAttribute";
    public const string AttributeName = "DeepLink";
}