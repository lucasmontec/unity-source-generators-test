﻿using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;

namespace SourceGeneratorsTest;

[Generator]
public class AsStringMethodGenerator : ISourceGenerator
{
    public void Execute(GeneratorExecutionContext context)
    {
        // Iterate through all syntax trees in the compilation
        foreach (var syntaxTree in context.Compilation.SyntaxTrees)
        {        
            var nodes = syntaxTree.GetRoot().DescendantNodes();

            // Find all class declarations
            var classNodes = nodes.OfType<ClassDeclarationSyntax>();

            foreach (var classNode in classNodes)
            {
                // Check if the class has the [AsString] attribute
                var hasAsStringAttribute = classNode.AttributeLists
                    .SelectMany(a => a.Attributes)
                    .Any(attr => attr.Name.ToString() == "AsString");

                if (!hasAsStringAttribute) continue;
                
                var className = classNode.Identifier.ValueText;
                var namespaceName = classNode.Parent is NamespaceDeclarationSyntax namespaceDecl
                    ? namespaceDecl.Name.ToString()
                    : null;

                var sourceBuilder = new StringBuilder($$"""
                                                            namespace {{namespaceName}}
                                                            {
                                                                public partial class {{className}}
                                                                {
                                                                    public string AsString()
                                                                    {
                                                                        return nameof({{className}});
                                                                    }
                                                                }
                                                            }
                                                        """);

                context.AddSource($"{className}_AsStringMethod.cs", SourceText.From(sourceBuilder.ToString(), Encoding.UTF8));
            }
        }
    }

    public void Initialize(GeneratorInitializationContext context) { }
}